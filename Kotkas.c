#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

uint16_t i, count  = 0;

void init( void );
void init_SystemClock_External( void ) ;

int main( void )
{
	init();
	sei(); // enable interrupts globally
	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	// Astonishingly tuneless demo...
	while(1){

	}
}

static inline void init_Timers( void ){
	PORTA_DIRSET = 0b00000100;
	PORTC_DIRSET = 0b00000001;
	PORTF_DIRSET = 0b00000110;
	
	TCD0.PERH = 0xff;
	TCD0.PERL = 0xff;
	TCD0.INTCTRLA = TC_OVFINTLVL_HI_gc ; // set overflow interrupt to low
	TCD0.CTRLA = TC_CLKSEL_DIV1024_gc ; // 1:1024 with main clock
	//TCF0.CTRLC = TC_CLKSEL_DIV1024_gc ; 
	//TCF0.PER = 624;
	TCC0.PER = 312; // lugemine hakkab nullist
	TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | TC0_CCAEN_bm;				// sets single slope mode http://electronics.stackexchange.com/questions/78111/atmel-xmega-pwm-problem
	//TCF0.CTRLD = TC_WGMODE_SINGLESLOPE_gc | TC0_CCCEN_bm;				// sets single slope mode http://electronics.stackexchange.com/questions/78111/atmel-xmega-pwm-problem
	//TCF0.CCC = 31;
	TCC0.CCA = 16;
	TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc ; // set overflow interrupt to low
	TCC0.CTRLA = TC_CLKSEL_DIV1024_gc ; // 1:1024 with main clock
	
	TCF0.PER = 312;
	TCF0.CTRLB = TC_WGMODE_SS_gc | TC0_CCBEN_bm |TC0_CCCEN_bm;
	TCF0_CCB = 16;
	TCF0_CCC = 24;
	TCF0.INTCTRLA = TC_OVFINTLVL_LO_gc ;
	TCF0.CTRLA = TC_CLKSEL_DIV1024_gc ;

}


ISR( TCD0_OVF_vect ){
	PORTA_OUTTGL = 0b00000100;
	i++;
	switch (i%8)
	{
		case 0:
		TCC0.CCA = 16;
		TCF0.CCB = 16;
		TCF0.CCC = 24;
		break;
		
		case 1:
		TCC0.CCA = 16;
		TCF0.CCB = 16;
		TCF0.CCC = 32;
		break;
		
		case 2:
		TCC0.CCA = 32;
		TCF0.CCB = 32;
		TCF0.CCC = 24;
		break;
		
		case 3:
		TCC0.CCA = 32;
		TCF0.CCB = 32;
		TCF0.CCC = 16;
		break;
		
		case 4:
		TCC0.CCA = 16;
		TCF0.CCB = 16;
		TCF0.CCC = 24;
		break;
	
		case 5:
		TCC0.CCA = 16;
		TCF0.CCB = 16;
		TCF0.CCC = 32;
		break;
		
		case 6:
		TCC0.CCA = 32;
		TCF0.CCB = 32;
		TCF0.CCC = 24;
		break;
		
		case 7:
		TCC0.CCA = 32;
		TCF0.CCB = 32;
		TCF0.CCC = 16;
		break;
	}
}

void init( void )
{
// Switch to use a 32Mhz clock...
//	init_SystemClock_Internal() ; // use this if you
	init_SystemClock_External() ; // Use this if you've got a 16Mhz crystal!
	init_Timers() ;
}

void init_SystemClock_External( void ){
// Use an external 16Mhz crystal and x 2 PLL to give a clock of 32Mhz

	// Enable the external oscillator
	OSC.XOSCCTRL = OSC_FRQRANGE_12TO16_gc | OSC_XOSCSEL_XTAL_16KCLK_gc ;
	OSC.CTRL |= OSC_XOSCEN_bm ;
	while( (OSC.STATUS & OSC_XOSCRDY_bm) == 0 ){} // wait until stable

	// Now configure the PLL to be eXternal OSCillator
	OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | 1;
	OSC.CTRL |= OSC_PLLEN_bm ; // enable the PLL...
	while( (OSC.STATUS & OSC_PLLRDY_bm) == 0 ){} // wait until stable

	// And now switch to the PLL as the clocksource
	CCP = CCP_IOREG_gc; // protected write follows	 
	CLK.CTRL = CLK_SCLKSEL_PLL_gc;
}


